﻿using System;
using UnityEngine;

public class DataSpot : MonoBehaviour
{
    [SerializeField]private int score;
    public static event Action<int> OnLanding;
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            OnLanding?.Invoke(score);
        }
    }
}
