﻿using UnityEngine;

public class Level : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.GetComponent<Ihittable>() == null) return;
        other.transform.GetComponent<Ihittable>().Damage();
    }
}