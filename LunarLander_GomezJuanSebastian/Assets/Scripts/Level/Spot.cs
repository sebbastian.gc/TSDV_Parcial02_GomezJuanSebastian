﻿using TMPro;
using UnityEngine;

public class Spot : MonoBehaviour
{
   private TextMeshProUGUI _text;
  

   private void Awake()
   {
      _text = GetComponent<TextMeshProUGUI>();
   }

   private void Start()
   {
      InvokeRepeating("ChangeColor",0.5f,0.5f);

   }

   private void ChangeColor()
   {
      _text.alpha  =  _text.alpha==1?0:1;
   }
}
