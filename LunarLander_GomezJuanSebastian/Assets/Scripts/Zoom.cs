﻿using System;
using UnityEngine;

public class Zoom : MonoBehaviour
{
    public static event Action<Vector3> OnZoomEnter;
    public static event Action OnZoomExit;
    public Transform _target;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
            OnZoomEnter?.Invoke(_target.position);
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
            OnZoomExit?.Invoke();
    }
}


