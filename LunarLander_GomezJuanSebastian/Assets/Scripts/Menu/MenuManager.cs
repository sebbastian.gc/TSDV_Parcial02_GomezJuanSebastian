﻿using UnityEngine;
using UnityEngine.Serialization;

public class MenuManager : MonoBehaviour
{
    public GameObject creditsScreen;
    public GameObject loaderScreen;
 
    public void LoadNextScene(string _scene)
    {
        LoaderManager.Get().LoadScene(_scene);
        UILoadingScreen.Get().SetVisible(true);
    }
    public void ChangeScreenCredits(bool isActive)
    {
        creditsScreen.SetActive(isActive);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
