﻿using MonoBehaviourSingletonScript;
using UnityEngine.UI;
public class UILoadingScreen : MonoBehaviourSingleton<UILoadingScreen>
{
	public Slider sliderLoad;

	public override void Awake()
	{
		base.Awake();
		gameObject.SetActive(false);
	}

	public void SetVisible(bool show)
	{
		gameObject.SetActive(show);
	}

	public void Update()
	{
		float loadingVal = LoaderManager.Get().loadingProgress * 100;
		sliderLoad.value = loadingVal;
		if (LoaderManager.Get().loadingProgress >= 1)
			SetVisible(false);
	}
}
