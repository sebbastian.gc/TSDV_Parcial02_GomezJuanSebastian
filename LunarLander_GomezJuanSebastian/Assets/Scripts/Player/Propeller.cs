﻿using UnityEngine;

public class Propeller : MonoBehaviour
{
    private ParticleSystem _effect;
    private void Awake()
    {
        _effect = GetComponent<ParticleSystem>();
        _effect.Stop();
    }

    private void FixedUpdate()
    {
        if (!Input.GetKey(KeyCode.Space))
            _effect.Play();
    }

    
}
