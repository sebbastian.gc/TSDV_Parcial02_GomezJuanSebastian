﻿using System;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D _rb;
    [SerializeField]private float verticalSpeed;
    [SerializeField]private float horizontalSpeed;
    [SerializeField]private float _fuel;
    public static event Action<float> OnCurrentFuel;
    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        OnCurrentFuel?.Invoke(_fuel);
    }
    private void FixedUpdate()
    {
        RotateShip();
        if (Input.GetKey(KeyCode.Space))
            Move();
    }
    private void Move()
    {
        _rb.AddForce(gameObject.transform.up*verticalSpeed);
        _fuel--;
        OnCurrentFuel?.Invoke(_fuel);
    }

    private void RotateShip()
    {
        if(Input.GetKey(KeyCode.A)&& _rb.rotation <= 90)
            _rb.rotation += horizontalSpeed;
        else if(Input.GetKey(KeyCode.D)&& _rb.rotation >= -90)
            _rb.rotation -= horizontalSpeed;
    }
}
