﻿using UnityEngine;

public class PlayerDeath : MonoBehaviour,Ihittable
{
    private Animator _animator;
    private Rigidbody2D _rb;
    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>(); 
    }
    public void Damage()
    {
        _rb.simulated = false;
        _animator.SetBool("Death",true);
    }
    public void Death() {
       
        Destroy(gameObject);
    }
}
