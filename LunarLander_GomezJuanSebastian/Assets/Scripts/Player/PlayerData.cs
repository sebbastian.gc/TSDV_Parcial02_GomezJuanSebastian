﻿using System;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    private float speedX;
    private float speedY;
    private int altitude;
    public static event Action<float,float,int> OnUpdateData;

    void Update()
    {
        UpdateData();
        OnUpdateData?.Invoke(speedX,speedY,altitude);
    }
    private void UpdateData()
    {
        if (transform.rotation.z >-0.09 && transform.rotation.z <0.09 )
            speedX= 0;
        else speedX = transform.position.x+10;
        
        speedY = transform.position.y +10;

        altitude = (int)speedY * 100;
    }
}
