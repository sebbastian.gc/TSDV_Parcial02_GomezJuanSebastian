﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIhud : MonoBehaviour
{
    public TextMeshProUGUI textClock;
    public TextMeshProUGUI textScore;
    public TextMeshProUGUI textVSpeed;
    public TextMeshProUGUI textHSpeed;
    public TextMeshProUGUI textAltitude;
    public TextMeshProUGUI textFuel;
    private TimeSpan timer;
    private bool timerBool;
    private float currentTime;
    
    private void Awake()
    {
        textClock.text = "00:00";
        timerBool = false;
        DataSpot.OnLanding += CurrentScore;
        PlayerData.OnUpdateData += CurrentDataPlayer;
        PlayerMovement.OnCurrentFuel += CurrentFuel;

    }

    private void OnDisable()
    {
        DataSpot.OnLanding -= CurrentScore;
        PlayerData.OnUpdateData -= CurrentDataPlayer;
        PlayerMovement.OnCurrentFuel -= CurrentFuel;
    }

    private void Start()
    {
        InitTimer();
    }
    
    public void ChangeScene(string scene)
    {
      SceneManager.LoadScene(scene);
    }

    private void CurrentFuel(float _fuel)
    {
        textFuel.text = "Fuel:" + _fuel;
    }

    private void CurrentDataPlayer(float x, float y, int alt)
    {
        textVSpeed.text = "V Speed:" + y;
        textHSpeed.text = "H Speed:" + x;
        textAltitude.text = "Altitude:" + alt;
    }
    private void CurrentScore(int _score)
    {
        textScore.text = "Score:" + _score;
    }
    
    private void InitTimer()
    {
        timerBool = true;
        currentTime = 0;

        StartCoroutine("UpdateTime");
    }

    private void EndTime()
    {
        timerBool = false;
    }

    private IEnumerator UpdateTime()
    {
        while (timerBool)
        {
            currentTime += Time.deltaTime;
            timer =TimeSpan.FromSeconds(currentTime);
            string timerStr =" "+ timer.ToString("mm':'ss");
            textClock.text = timerStr;
            yield return null;
        }
    }

    
    
}
