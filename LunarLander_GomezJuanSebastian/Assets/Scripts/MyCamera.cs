﻿using UnityEngine;

public class MyCamera : MonoBehaviour
{
    private Camera _cam;
    private void Awake()
    {
        _cam = GetComponent<Camera>();
        _cam.enabled = false;
        Zoom.OnZoomEnter += ZoomCamera;
        Zoom.OnZoomExit += NormalSight;

    }

    private void OnDisable()
    {
        Zoom.OnZoomEnter -= ZoomCamera;
        Zoom.OnZoomExit -= NormalSight;
    }

    private void ZoomCamera(Vector3 pos)
    {
        gameObject.transform.position = pos;
        _cam.enabled = true;
    }

    private void NormalSight()
    {
        _cam.enabled = false;
    }
}
